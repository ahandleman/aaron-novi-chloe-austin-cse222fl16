

#include <SimbleeBLE.h>

const int red = 2;
const int green = 3;
const int blue = 4;
int redv;
int bluev;
int greenv;
unsigned long count = 0;

void setup() {
  // Setup Hardware
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);

  // Setup Serial for debugging
  Serial.begin(9600);
  Serial.println("Starting");

  // Setup BLE service
  SimbleeBLE.deviceName = "ACN";
  SimbleeBLE.begin();
}

void loop() {
  //Serial.println("Loop Repeating");
  Aaron_loop_checks();
  Simblee_ULPDelay(1000);
  count++;
  SimbleeBLE.sendInt(count);
}

void SimbleeBLE_onConnect()
{
  Serial.println("onConnect");

}

void SimbleeBLE_onDisconnect()
{
  Serial.println("onDisconnect");
}

void SimbleeBLE_onReceive(char data[], int len)
{
  Serial.println("onReceive");
  Serial.print("Recieved Data: ");
  for (int i = 0; i < len; i++) {
    Serial.print("[ " + String(data[i], HEX) + " ] ");
  }
  Serial.println("");
  if (len == 1) {
    if (data[0]) {
      Serial.println("Light ON");
      digitalWrite(red, HIGH);
      digitalWrite(green, HIGH);
      digitalWrite(blue, HIGH);
    }
    else {
      Serial.println("Light OFF");
      digitalWrite(red, LOW);
      digitalWrite(green, LOW);
      digitalWrite(blue, LOW);
    }
  } else if (len == 3) {
    redv = data[0];
    greenv = data[1];
    bluev = data[2];
    analogWrite(red, redv);
    analogWrite(green, greenv);
    analogWrite(blue, bluev);
  }
}

bool Aaron_debug_on = false;
int Aaron_loop_checks() {
  if (Serial.available() <= 0) {
    return 0;
  }
  String s = "";
  while (Serial.available() > 0) {
    char mychar;
    mychar = (char)Serial.read();
    if (mychar != '\n') {
      s += mychar;
    }
  }
  s.toUpperCase();
  Aaron_debug("Input: " + s);
  switch (s[0]) {
    case 'R':
      Aaron_restart();
      break;
    case 'D':
      Aaron_debug_on = !Aaron_debug_on;
      String temp1 = "Debug is now ";
      temp1 += (Aaron_debug_on ? "on." : "off.");
      Serial.println(temp1);
      break;
  }
}

int Aaron_restart() {
  Aaron_debug("Restart");
  delay(100);
  Simblee_systemReset();
}

int Aaron_debug(char* s) {
  if (Aaron_debug_on) {
    Serial.println(s);
  }
}

int Aaron_debug(String s) {
  if (Aaron_debug_on) {
    Serial.print("ADB:  ");
    Serial.println(s);
  }
}

