/*
This sketch is based on RF Digital's LedSimple sketch (in Simblee Examples)
*/
#include <SimbleeForMobile.h>  //Includes the package

// pin 3 on the RGB shield is the green led
// (can be turned on/off from the iPhone app)
int led;
enum fsmStates {brightenS, dimS, neutral};
int curState = neutral;
// Create some ID variables to keep track of items on the screen.
uint8_t ui_button_on;
uint8_t ui_button_off;
uint8_t ui_button_aoff;
uint8_t ui_button_change;
uint8_t ui_rect;
unsigned long acc = 0;
int led_state = 0; 
color_t bColor = GREEN;


void setup() {
  // led turned on/off from the app
  pinMode(3, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
  led = 3;
  digitalWrite(led, led_state);
  // TODO: Change the "ledbtn" string to a unique 2-5 letter code for your group 
  SimbleeForMobile.advertisementData = "wwwww";  
  Serial.begin(9600);
  SimbleeForMobile.begin();
}

void loop() {
  // process must be called in the loop for SimbleeForMobile
  SimbleeForMobile.process();
  if (millis() - acc > 500) {
    switch(curState){
      case brightenS:
        brighten();
        break;
      case dimS:
        dim();
        break;
      default:
        break;
    }
  }
  delay(100);
}

void ui() //Runs when new device connects
{
  SimbleeForMobile.beginScreen(WHITE);
  // Create a button and keep track of it's ID
  ui_button_on = SimbleeForMobile.drawButton(120, 100, 100, "On", bColor);
  ui_button_off = SimbleeForMobile.drawButton(120, 180, 100, "Off", bColor);
  ui_button_change = SimbleeForMobile.drawButton(120, 260, 100, "Change", bColor);
  ui_button_aoff = SimbleeForMobile.drawButton(120, 340, 100, "All Off", BLACK);
  
  // Set the button to trigger UI events
  SimbleeForMobile.setEvents(ui_button_on, EVENT_RELEASE);
  SimbleeForMobile.setEvents(ui_button_off, EVENT_RELEASE);
  SimbleeForMobile.setEvents(ui_button_change, EVENT_RELEASE);  
  SimbleeForMobile.setEvents(ui_button_aoff, EVENT_RELEASE);
  //Serial.println("test");
  SimbleeForMobile.endScreen();
}

void ui_event(event_t &event)
{
  // Check if the button was pressed!
  //Serial.println("test");
  if (event.id == ui_button_on) {
    if (event.type == EVENT_PRESS) {
       curState = brightenS;
    } else if (event.type == EVENT_RELEASE) {
       curState = neutral;
    }
  }
  if (event.id == ui_button_off) {
    if (event.type == EVENT_PRESS) {
       curState = dimS;
    } else if (event.type == EVENT_RELEASE) {
       curState = neutral;
    }
  }
  if (event.id == ui_button_change) {
    if (event.type == EVENT_RELEASE) {
       if (led == 2) {
         led = 3;
         bColor = GREEN;
       } else if (led == 3) {
         led = 4;
         bColor = BLUE;
       } else if (led == 4) {
         led = 2;
         bColor = RED;
       }
       SimbleeForMobile.updateColor(ui_button_on, bColor);
       SimbleeForMobile.updateColor(ui_button_off, bColor);
       SimbleeForMobile.updateColor(ui_button_change, bColor);
    }
  }
  if (event.id == ui_button_aoff) {
    if (event.type == EVENT_RELEASE) {
       led_state = false;
       digitalWrite(2, led_state);
       digitalWrite(3, led_state);
       digitalWrite(4, led_state);
    }
  }
}

void brighten(){
  if (led_state <= 252)
  led_state += 3;
  analogWrite(led , led_state);
}

void dim(){
  if (led_state >= 3)
  led_state -= 3;
  analogWrite(led , led_state);
}


