// Notice the include is SimbleeBLE NOT SimbleForMobile
/* Initial Code written in class by Chloe Wright, Aaron Handleman, and Novi Wang
 * 
 * Features included in group code:
 * Writing is done to UUID: 2d30c083-f39f-4ce6-923f-3484ea480596
 * Reading (supports notifications) is done from UUID: 2d30c082-f39f-4ce6-923f-3484ea480596)
 * 
 * Writing Protocol: Write some number of bytes, the first of which is a tag byte determining
 * what the Simblee does and how it uses the remaining bytes.
 * 
 * Tag (0x01), requires two bytes: Sets the value of the red LED to the value of the second byte.
 * Tag (0x02), requires two bytes: Sets the value of the blue LED to the value of the second byte.
 * Tag (0x03), requires two bytes: Sets the value of the green LED to the value of the second byte.
 * Tag (0x04), requires four bytes: Sets the value of the red LED to the value of the second byte, 
 * the blue LED to the value of the third byte, and the green LED to the value of the fourth byte.
 * Tag (0x05), requires one byte: Instructs the Simblee to send a packet with the current value of 
 * each LED
 * Tag (default), acts on any non-defined tag: Print the sent bytes to the Serial Monitor.  Most 
 * useful for debugging purposes.
 * 
 * 
 * Reading Protocol:  The Simblee will send between some number of bytes, the first of which is a
 * tag byte dictating how the remaining bytes should be interpretted (or if there are no other bytes
 * it explains what recieving that notification means)
 * 
 * Tag (0x01), sends one byte: This packet is sent whenever the button is pushed, it contains no
 * other information.
 * Tag (0x05), sends four bytes: The second byte is the current value of the red LED, the third byte 
 * is the current value of the blue LED, and the fourth byte is the current value of the green LED.
 * 
 * End of in-class group code documentation.  Significant things written as a group marked with comments.
 */
#include <SimbleeBLE.h>
int red = 0;
int blue = 0;
int green = 0;
int button = 0;
char* outData;
void setup() {

  
  // TODO: Change the "ledbtn" string to a unique 2-5 letter code for your group 
  SimbleeBLE.advertisementData = "wwwww";
  Serial.begin(9600);
  pinMode(3, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, INPUT);
  // start the BLE stack
  SimbleeBLE.begin();
}

void loop() {
  //Start of group code, checks if button has been pressed and if so send a packet with the button tag (0x01)
  int temp = digitalRead(5);
  if (button != temp) {  //Checks to see if the button has changed, so we don't continuously send packets when pressed
    button = temp;
    if (button == 1) { //Only sends on press, not release
      outData = new char[1];
      outData[0] = 1;
      SimbleeBLE.send(outData, 1);
    }
  }
  //end of group code 
}

void SimbleeBLE_onReceive(char data[], int len) {
  //If statement for 0x01 through 0x05 and the default case written as a group.
  if (len < 5) {
    if (data[0] == 0x01 && len >= 2) {
       red = data[1];
       writeLEDs();
    } else if (data[0] == 0x02 && len >=2) {
       blue = data[1];
       writeLEDs();
    } else if (data[0] == 0x03 && len >=2) {
       green = data[1];
       writeLEDs();
    } else if (data[0] == 0x04 && len >= 4) {
       red = data[1];
       blue = data[2];
       green = data[3]; 
       writeLEDs();
    } else if (data[0] == 0x05){
      outData = new char[4];
      outData[0] = 0x05;
      outData[1] = red;
      outData[2] = green;
      outData[3] = blue;
      SimbleeBLE.send(outData, 4);
    } else {
     for (int i = 0; i < len; i++) {
       char cur = data[i];
       Serial.print(cur, HEX);
       Serial.print(" ");
     }
     Serial.println(" ");
    }
    
  }
}

void writeLEDs() {
    analogWrite(2, red);
    analogWrite(3, green);
    analogWrite(4, blue);
}

